﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            String str;
            Console.Out.WriteLine("Hello World!");
            Console.Out.WriteLine(); // blank line
            Console.Out.WriteLine(); // blank line
            Console.Out.WriteLine("Enter a line");
            str = Console.In.ReadLine();
            Console.Out.WriteLine(str);
            Console.Out.WriteLine("Press enter to exit.");
            str = Console.In.ReadLine();
        }
    }
}
