﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="Report" Theme="Standard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:DropDownList ID="ReportSelectionList" runat="server" OnSelectedIndexChanged="index_changed" AutoPostBack="true">
    <asp:ListItem  Text="Show 3" Value="3" Selected="True" Enabled="True" />
    <asp:ListItem  Text="Show 5" Value="5" Selected="False" Enabled="True" />
    <asp:ListItem  Text="Show 10" Value="10" Selected="False" Enabled="True" />
</asp:DropDownList>
<br />
<br />
<asp:GridView ID="TemplatedGridView" runat="server" AutoGenerateColumns="false" ShowHeader="true" ShowFooter="true">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <b>User Id</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                           <%# Eval("UserId") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <b>First Name</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                            <%# Eval("FirstName") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <b>Last Name</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                            <%# Eval("LastName") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                        <HeaderTemplate>
                            <b>Birthday</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                            <%# Eval("Birthday") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                        <HeaderTemplate>
                            <b>Position</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                           <%# Eval("Position") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                        <HeaderTemplate>
                            <b>Telephone</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                            <%# Eval("Telephone") %>
                        </ItemTemplate>
                    </asp:TemplateField>


                     <asp:TemplateField>
                        <HeaderTemplate>
                            <b>Program</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                            <%# Eval("Program") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                        <HeaderTemplate>
                            <b>Games Played</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                            <%# Eval("GamesPlayed") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                     <asp:TemplateField>
                        <HeaderTemplate>
                            <b>Decription</b>
                        </HeaderTemplate>
                        
                        <ItemTemplate>
                            <%# Eval("Description") %>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

</asp:Content>

