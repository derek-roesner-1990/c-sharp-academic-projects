﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Report : System.Web.UI.Page
{

    Lab7DataContext Lab7db = new Lab7DataContext();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack) // determine if this is the very first time the page is loaded, if it is -- bind the content
        {
            TemplatedGridView.DataSource = Lab7db.Users;       // the collection we are binding
            TemplatedGridView.DataBind();                     // bind the data

        }
    }

    protected void index_changed(object sender, EventArgs e)
    {
        TemplatedGridView.DataSource = (from p in Lab7db.Users select p).Take(Int32.Parse(ReportSelectionList.SelectedValue));
        TemplatedGridView.DataBind(); // bind the data
    }


}