﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
        
        A:link { text-decoration : none }
        
        body
        {
            background-color:Black;
            text-align:center;
        }
        
        #container
        {
           padding:15% 0 15% 0;  
        }
        
        #helloLink
        {
            color:White;
            font-size:100px;   
        }
        
        #registerLink
        {
            background-color:White;
            color:Black;
            font-size:100px;
        }
    
    </style>
    
    <title>Menu</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <div id="helloLink"> 
            <asp:HyperLink runat="server" id="helloLink" Text="Hello!" NavigateUrl="HelloWorld.aspx"/> 
        </div>
        <br />
        <div id="registerLink">
            <asp:LinkButton runat="server" id="registerLink" Text="Register?" OnClick ="registerLink_Click"/> 
        </div>
        
    </div>
    </form>
</body>
</html>

<%--The link to HelloWorld.aspx: 
    Use the ASP Hyperlink control to navigate to this page
    Properties of note
    NavigateUrl
    Text

    The link to ‘Register.aspx’:
    Use the ASP LinkButton control to navigate to this page
    Use the OnClick event and create a server event that will send the user to Register.aspx
    Web pages implement the same web objects used in modern browsers and the Page object has access to them
    From your new event in Default.aspx.cs use this.Response.Redirect() to send the user to Register.aspx 
    
    The pages Default and Register should also contain links that go back to Default.aspx. --%>
    </div>