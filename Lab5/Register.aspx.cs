﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            submit.Click += new EventHandler(submitBtn_Click);
        }

        public void submitBtn_Click(Object sender, EventArgs e)
        {
            List<string> userAttribs = new List<string>();

            Lab5.Registrant user = new Lab5.Registrant();
            submit.Enabled = false;
            submit.Text = "Thanks!";
            
            if (fname.Text != "First Name")
            {
                user.FirstName = fname.Text;
            }
            
            if (lname.Text != "Last Name")
            {
                user.LastName = lname.Text;
            }
            
            if(bdayYear.Text != "yyyy" && bdayMonth.Text != "mm" && bdayDay.Text != "dd")
            {
                user.Birthday = bdayYear.Text + bdayMonth.Text + bdayDay.Text;
            }
            
            user.Position = posList.SelectedItem.Text;

            if(tel.Text != "xxx-xxx-xxxx")
            {
                tel.Text = tel.Text.Replace("-", "");
                user.Telephone = tel.Text;
            }

            if(program.Text != "Program Name")
            {
                user.Program = program.Text;
            }

            userAttribs.Add(user.FirstName);
            userAttribs.Add(user.LastName);
            userAttribs.Add(user.Birthday);
            userAttribs.Add(user.Position);
            userAttribs.Add(user.Telephone);
            userAttribs.Add(user.Program);

            if (checkForEmpty(userAttribs) == false)
            {
                user.InsertRegistrant();
                /*will redirect to default.aspx*/        
            }
            else
            {
                submit.Enabled = true;
                submit.Text = "Submit";
                fname.Text = "First Name";
                lname.Text = "Last Name";
                bdayYear.Text = "yyyy";
                bdayMonth.Text = "mm";
                bdayDay.Text = "dd";
                posList.SelectedIndex = 0;
                tel.Text = "xxx-xxx-xxxx";
                program.Text = "Program Name";
            }

        }

        /*checkFields()
         *Checks to see if all attributes of a registrant object are initialized
          Returns true is an empty field is found*/
        public bool checkForEmpty(List<string> toCheck)
        {
            for(int i = 0; i < toCheck.Count; i++){
                if(toCheck[i] == ""){
                    return true;
                }
            }
            return false;
        }

    }