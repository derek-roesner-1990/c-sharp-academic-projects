﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lab5
{
    public class Registrant
    {
    // holds the objects connection string
    private string connectionString = ConnectionString.connectionString;


    public int UserId
    {
        get;
        set;
    }

    public string FirstName
    {
        get;
        set;
    }

    
    public string LastName
    {
        get;
        set;
    }

    public string Birthday
    {
        get;
        set;
    }

    public string Position
    {
       get;
       set;
    }

    public string Telephone
    {
        get;
        set;
    }

    public string Program
    {
        get;
        set;
    }

    /// <summary>
    /// Constructor, default, creates a new empty Person object
    /// </summary>
    public Registrant()
    {
        UserId = 0;
        FirstName = string.Empty;
        LastName = string.Empty;
        Birthday = string.Empty;
        Position = string.Empty;
        Telephone = string.Empty;
        Program = string.Empty;
    }

    /// <summary>
    /// Method, inserts a person to the database
    /// </summary>
    public void InsertRegistrant()
    {
        
        string sqlQuery = "INSERT INTO [Registrant] ( FirstName, LastName, Birthday, Position, Telephone, Program ) VALUES ('" + this.FirstName + "','" + this.LastName + "','" + this.Birthday + "','" + this.Position + "','" + this.Telephone + "','" + this.Program + "')";
      
       SqlConnection connection = new SqlConnection(this.connectionString);
       SqlCommand command = new SqlCommand(sqlQuery, connection);

        try
        {
            connection.Open();

            command.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Method, updates this person object in the database
    /// </summary>
    public void UpdateRegistrant(string FirstName, string LastName, string Birthday, string Position, string Telephone, string Program )
    {
        string sqlQuery = "UPDATE [Registrant] SET FirstName = '" + FirstName + "',  LastName = '" + LastName + "', Birthday = '" + Birthday + "', Position = '" + Position + "', Telephone = '" + Telephone + "', Program = '" + Program + "'";
        
        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);

        try
        {
            connection.Open();

            command.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Method, deletes this person from the database
    /// </summary>
    public void DeleteRegistrant()
    {
        string sqlQuery = "DELETE FROM [Registrant] WHERE UserId ='" + this.UserId + "'";

        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);
        SqlDataReader reader = null;

        try
        {
            connection.Open();

            command.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (reader != null)
                reader.Close();

            connection.Close();
        }
    }

    /// <summary>
    /// Method, gets a person from the database using the person id provided
    /// </summary>
    public void GetRegistrant(int UserId)
    {
        
        string sqlQuery = "SELECT * FROM [Registrant] WHERE UserId ='" + UserId + "'";
        
        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);
        SqlDataReader reader = null;

        try
        {
            connection.Open();

            reader = command.ExecuteReader();

            if (reader.Read())
            {
                this.UserId = UserId;
                this.FirstName = reader["FirstName"].ToString();
                this.LastName = reader["LastName"].ToString();
                this.Birthday = reader["Birthday"].ToString();
                this.Position = reader["Position"].ToString();
                this.Telephone = reader["Telephone"].ToString();
                this.Program = reader["Program"].ToString();
            }
            else
                throw new Exception("Registrant Not Found");
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (reader != null)
            {
                reader.Close();
                connection.Close();
            }
        }
    }

  /*  /// <summary>
    /// Method, gets all the people from the database
    /// </summary>
    /// <returns></returns>
    public List<Registrant> GetFiles()
    {
        List<Registrant> fileCollection = new List<Registrant>();

        string sqlQuery = "SELECT * FROM [Registrant]";

        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);
        SqlDataReader reader = null;

        try
        {
            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                Registrant f = new Registrant();
                f.FileId = Convert.ToInt32(reader["FileId"]);
                f.Name = reader["Name"].ToString();
                f.Path = reader["Path"].ToString();
                f.Length = Convert.ToInt32(reader["Length"]);

                registrantCollection.Add(f);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (reader != null)
                reader.Close();

            connection.Close();
        }

        return registrantCollection;
    }*/

    }//end of class
}//end of namespace access
