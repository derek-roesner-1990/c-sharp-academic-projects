﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HelloWorld.aspx.cs" Inherits="HelloWorld" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <style type = "text/css">
    
    A:link 
    {
        text-decoration: none;
        color:red;  
    }
    
    body
    {
      background-color:Black;
      margin: 0 10 0 10;
      color:White;
    }
    
    h1
    {
      background:Orange;
      color:Lime;  
    }
    
    #container
    {
      height:65px;
      padding:18% 0 18% 0; 
    }
    
    #header
    {
     height:60%;
     text-indent:5px;
    }
    
    #hello
    {
      background-color:Blue;
      color:Yellow;
      height:30%; 
      text-indent:5px; 
      /*text-align:center;*/
    }
    
    #menuLink
    {
      color:Red;
      text-align:right;  
    }
    
    </style>
    <title>Hello ASP.NET World!</title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="container">
        <div id ="header">
            <h1>Hello World</h1>
        </div>
        <div id ="menuLink">
            <asp:HyperLink runat="server" id="MenuLink" text="Back to Menu" NavigateUrl="Default.aspx"/>
        </div>
        <div id="hello">
            This is my first ASP.NET Website
        </div>
    </div>
    </form>
</body>
</html>


<%--Create a new web form called ‘HelloWorld.aspx’. 
Put the word ‘Hello World’ in the largest heading tag 
Put the words ‘This is my first ASP.NET Website’ in a div
Create Internal CSS to modify your page
Change the largest heading tag to be the color Orange
Change the background color of the div blue and the font yellow.--%>