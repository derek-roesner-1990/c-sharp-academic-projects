﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style type="text/css">
      
      A:link  
      {
          text-decoration:none;
          color:White;
      }
      
      body
      {
        text-align:center;
        background-color:Black;
        color:White;
      }
      
      #infoPanel
      {
        padding:12% 0 12% 0;
      }
      
      #menuLink
      {
        font-style:oblique;
      }
      
      
     </style>
<title>Welcome to some group!</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div id="infoPanel">
        <div><h3>Please enter your profile information</h3></div>
        <div>Family Name</div>
        <asp:TextBox runat="server" id="fname"  Text="First Name" OnClick="this.value=''"/> 
        <asp:TextBox runat="server" id="lname"  Text="Last Name" OnClick="this.value=''"/>
        <br /><br />
        <div>Birthday</div>
            <asp:TextBox runat="server" id="bdayYear"  Text="yyyy" OnClick="this.value=''"/>
            <asp:TextBox runat="server" id="bdayMonth"  Text="mm" OnClick="this.value=''"/>
            <asp:TextBox runat="server" id="bdayDay"  Text="dd" OnClick="this.value=''"/>
         <br /> <br />
            <div>
                Position
                <asp:DropDownList runat="server" id="posList">
                    <asp:ListItem Text="Student"></asp:ListItem>
                    <asp:ListItem Text="Staff"></asp:ListItem>
                </asp:DropDownList>
                <br /> <br />
             </div>
            <div>
                Telephone Number
                <asp:TextBox runat="server" id="tel"  Text="###-###-###" OnClick="this.value=''"/>
                <br /> <br />
            </div>
            <div>Program of Study
            <asp:TextBox runat="server" id="program"  Text="Program Name" OnClick="this.value=''"/>
            </div>
            <br />
            <asp:Button runat="server" id="submit" Text="Submit"/>
            <br /> <br /> <br /> <br />
            <asp:HyperLink runat="server" id="menuLink" Text="Back To Menu" NavigateUrl="Default.aspx"/>                                        
        </div>
        </form>
</body>
</html>
  
    <%-- 
        Welcome to 
    
         First Name - asp:TextBox
         Last Name - asp:TextBox
         Birthday - asp:TextBox
         A drop down list that indicates whether the person is Student or Staff - asp:DropDownList
         Telephone Number - asp:TextBox
         Program of Study - asp:TextBox
         
         Button to submit 
     --%>

   

