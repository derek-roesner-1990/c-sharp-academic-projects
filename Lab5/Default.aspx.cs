﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        registerLink.Click += new EventHandler(registerLink_Click);
    }

    public void registerLink_Click(object sender, EventArgs e)
    {
        this.Response.Redirect("Register.aspx");
    }
}