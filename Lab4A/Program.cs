﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace lab4A
{
    class Lab4a
    {
        public lab4A.DataClasses1DataContext db = new lab4A.DataClasses1DataContext();
        public int fileAmt = 0;
        public const int FILE_MAX = 50;

        static void Main(string[] args)
        {
            Lab4a lab4a = new Lab4a();
            /*string path = "C:\\Users\\Derek Roesner\\Desktop\\.Net Programming";*/
            /*string path = "C:\\\\Windows";*/
            string path = "C:\\Users\\Derek Roesner\\Desktop\\.Net Programming";

            Console.Out.WriteLine("Crawling {0}.....", path);

            lab4a.crawl(path);

            File result = (from f in lab4a.db.Files select f).First();

            Console.Out.WriteLine("First file added to database");
            lab4a.printFile(result);

            Console.Out.WriteLine("Deleting the first file from the database....");
            lab4a.db.Files.DeleteOnSubmit(result);
            lab4a.db.SubmitChanges();

            Console.Out.WriteLine("New first file");
            result = (from f in lab4a.db.Files select f).First();
            lab4a.printFile(result);


            Console.Out.WriteLine("Changing name of first file in database....");
            result.Name = "changed";
            lab4a.db.SubmitChanges();
            var allFiles = (from f in lab4a.db.Files select f);
            Console.Out.WriteLine("Printing the details of all files in database....");
            foreach (File f in allFiles)
                lab4a.printFile(f);
        }

        public void crawl(String currentDirectory)
        {
            int i;
            String[] files = null;
            String[] directories = null;
            System.IO.FileInfo fInfo = null;
            System.IO.DirectoryInfo dInfo = null;
            

            try
            {
                /*Attempt all these in a row since they all need to be checked for the same error.*/
                directories = Directory.GetDirectories(currentDirectory);
                files = Directory.GetFiles(currentDirectory);
                dInfo = new DirectoryInfo(currentDirectory);
            }
            catch (UnauthorizedAccessException uae)
            {
                Console.Out.WriteLine(String.Format("Directory {0,-15} -- Cannot Access Directory --", currentDirectory));
            }

            if (directories != null)
            {

                for (i = 0; i < directories.Length; i++)
                {
                    if (fileAmt < FILE_MAX)
                    {
                        crawl(directories[i]);
                    }
                }

            }
            if (files != null)
            {

                for (i = 0; i < files.Length; i++)
                {
                    try
                    {
                        fInfo = new FileInfo(files[i]);
                    }
                    catch (UnauthorizedAccessException uae)
                    {
                        Console.Out.WriteLine(String.Format("File {0,-15} -- Cannot Access File --", files[i]));
                    }
                    if (fInfo != null)
                    {
                        if (fileAmt < FILE_MAX)
                        {
                            File currentFile = new File();
                            currentFile.Name = fInfo.Name;
                            currentFile.Path = fInfo.FullName;
                            currentFile.Length = fInfo.Length;
                            db.Files.InsertOnSubmit(currentFile);
                            db.SubmitChanges();
                            fileAmt++;
                        }else{
                             return;
                        }
                       
                       
                    }
                }
            }
        }

        public void printFile(File f)
        {
            Console.Out.WriteLine("{0,-40}{1,-10}{2,-50}\n", f.Name, f.Length, f.Path);
        }

    }
}
