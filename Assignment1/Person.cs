﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Assignment1
{
    public class Person
    {
        public string connectionString = Assignment1.ConnectionString.ATMconnectionString;

        public int PersonId
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string LastName
        {
            get;
            set;
        }

        public string EmailAddress
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }

        public string SINNumber
        {
            get;
            set;
        }

        public Person()
        {
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.EmailAddress = string.Empty;
            this.Password = string.Empty;
            this.SINNumber = string.Empty;
        }

        public void InsertPerson()
        {
            string sqlQuery = "INSERT INTO [People] (FirstName,LastName,EmailAddress,Password,SINNumber) VALUES ('" + this.FirstName + "','" + this.LastName + "','" + this.EmailAddress + "','" + this.Password + "','" + this.SINNumber + "')";
            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.Out.WriteLine("\nCannot insert this person record into the database");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void UpdatePerson(string firstName, string lastName, string email, string password, string sin)
        {
            string sqlQuery = "UPDATE [People] SET FirstName = '" + firstName + "', LastName = '" + lastName + "', EmailAddress = '" + email + "', Password = '" + password + "', SINNumber = '" + sin + '"';

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nCannot update this student record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void DeletePerson()
        {
            string sqlQuery = "DELETE FROM [People] WHERE PersonId ='" + this.PersonId + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nCannot delete this person record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
               connection.Close();
            }
        }

        public void GetPersonById(int personId)
        {

            string sqlQuery = "SELECT * FROM [People] WHERE PersonId ='" + personId + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    this.PersonId = personId;
                    this.FirstName = reader["FirstName"].ToString();
                    this.LastName = reader["LastName"].ToString();
                    this.EmailAddress = reader["EmailAddress"].ToString();
                    this.Password = reader["Password"].ToString();
                    this.SINNumber = reader["SINNumber"].ToString();
                }
                else { Console.Out.WriteLine("\nUnable to retrieve user record for personId {0}", personId); }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nError retrieving user record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }
        }


        public void GetPersonByName(string firstName,string lastName)
        {

            string sqlQuery = "SELECT * FROM [People] WHERE FirstName ='" + firstName + "' AND LastName ='" + lastName + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    this.PersonId =  Convert.ToInt32(reader["PersonID"]);
                    this.FirstName = reader["FirstName"].ToString();
                    this.LastName = reader["LastName"].ToString();
                    this.EmailAddress = reader["EmailAddress"].ToString();
                    this.Password = reader["Password"].ToString();
                    this.SINNumber = reader["SINNumber"].ToString();
                }
                else
                    Console.Out.WriteLine("\nCannot retrieve record details for the user: {0} {1}", firstName, lastName);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nError retrieving records details for the user: {0} {1}",firstName,lastName);
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }
        }

        public List<Person> GetPeople()
        {
            List<Person> peopleCollection = new List<Person>();

            string sqlQuery = "SELECT * FROM [People]";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Person p = new Person();
                    p.PersonId = Convert.ToInt32(reader["PersonId"]);
                    p.FirstName = reader["FirstName"].ToString();
                    p.LastName = reader["LastName"].ToString();
                    p.EmailAddress = reader["EmailAddress"].ToString();
                    p.Password = reader["Password"].ToString();
                    p.SINNumber = reader["SINNumber"].ToString();

                    peopleCollection.Add(p);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nCannot retrieve all person records");
                Console.Out.WriteLine("\nDetails: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }

            return peopleCollection;
        }

        public void Login(string emailAddress, string password)
        {
            string sqlQuery = "SELECT * FROM [People] WHERE EmailAddress ='" + emailAddress + "'AND Password = '" + password + "'";
            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader;
            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                   
                    this.PersonId = Convert.ToInt32(reader["PersonId"]);
                    this.FirstName = reader["FirstName"].ToString();
                    this.LastName = reader["LastName"].ToString();
                    this.EmailAddress = reader["EmailAddress"].ToString();
                    this.Password = reader["Password"].ToString();
                    this.SINNumber = reader["SINNumber"].ToString();

                }
                else
                {
                    Console.Out.WriteLine("\nUser Not Found");
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nError logging into user account using given credentials");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

    }
}
