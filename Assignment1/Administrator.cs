﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Assignment1
{
    public class Administrator
    {
        public string connectionString = ConnectionString.ATMconnectionString;

       public string username
        {
            get;
            set;
        }

        public string password
        {
            get;
            set;
        }

        public Administrator()
        {
            this.username = string.Empty;
            this.password = string.Empty;
        }

        public void InsertAdministrator()
        {
            string sqlQuery = "INSERT INTO [Administrators] (Username,Password) VALUES ('" + this.username + "','" + this.password + "')";
            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.Out.WriteLine("\nCannot insert this admininistrator record into database");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void UpdateAdministrator(string username, string password)
        {
            string sqlQuery = "UPDATE [Administrators] SET Userame = '" + username + "', Password = '" + password + '"';

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nCannot update this admininistrator record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void DeleteAdministrator()
        {
            string sqlQuery = "DELETE FROM [Administrators] WHERE Username ='" + this.username + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nCannot delete this admininistrator record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void GetAdministrator(string username)
        {

            string sqlQuery = "SELECT * FROM [Administrators] WHERE Username ='" + username + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    this.username = username;
                    this.password = reader["Password"].ToString();
                }
                else
                {
                    Console.Out.WriteLine("\nUnable to retrieve administrator record with username {0}", username);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nError retrieving administrator record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }
        }

        public List<Administrator> GetAdministrators()
        {
            List<Administrator> AdminCollection = new List<Administrator>();

            string sqlQuery = "SELECT * FROM [Administrators]";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Administrator a = new Administrator();
                    a.username = reader["Username"].ToString();
                    a.password = reader["Password"].ToString();
                   
                    AdminCollection.Add(a);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nUnable to retrieve all administrator records from database");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }

            return AdminCollection;
        }

        public void Login(string username, string password)
        {
            string sqlQuery = "SELECT * FROM [Administrators] WHERE Username ='" + username + "' AND Password = '" + password + "'";
            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader;
            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    this.username = reader["Username"].ToString();
                    this.password = reader["Password"].ToString();
                }
                else
                {
                    Console.Out.WriteLine("\nAdministrator record not found in the database");
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nError logging into administrator account using given credentials");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
         }
    }
}
