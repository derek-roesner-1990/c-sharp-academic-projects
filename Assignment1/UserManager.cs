﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.IO;
using System.Data.SqlClient;

namespace Assignment1
{
    class UserManager
    {
        public Person user = null;

        public UserManager()
        {
            
        }

        public UserManager(string emailAddress, string password)
        {
            user = new Person();
            user.Login(emailAddress,password);
        }

        public void createUser()
        {
            string hashedString;
            
            user = new Person();
            Console.Out.WriteLine("Please enter account information");
            while (true)
            {
                Console.Out.Write("First Name: ");
                try
                {
                    user.FirstName = Console.In.ReadLine();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("Error reading first name");
                    Console.Out.WriteLine("Details: {0}", e.Message);
                    continue;
                }

                if (user.FirstName.Length > 50)
                {
                    Console.Out.WriteLine("First name cannot be longer than 50 characters.");
                    continue;
                }
                break;
            }

            while (true)
            {
                Console.Out.Write("Last Name: ");
                try
                {
                    user.LastName = Console.In.ReadLine();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("Error reading last name");
                    Console.Out.WriteLine("Details: {0}", e.Message);
                    continue;
                }

                if (user.LastName.Length > 50)
                {
                    Console.Out.WriteLine("Last name cannot be longer than 50 characters.");
                    continue;
                }
                break;
            }

            while (true)
            {
                Console.Out.Write("Email Address: ");
                try
                {
                    user.EmailAddress = Console.In.ReadLine();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("Error reading email address");
                    Console.Out.WriteLine("Details: {0}", e.Message);
                    continue;
                }

                if (user.EmailAddress.Length > 255)
                {
                    Console.Out.WriteLine("The email address entered cannot be longer than 255 characters.");
                    continue;
                }
                break;
            }

            while (true)
            {
                Console.Out.Write("Password: ");
                try
                {
                    user.Password = Console.In.ReadLine();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("Error reading password");
                    Console.Out.WriteLine("Details: {0}", e.Message);
                    continue;
                }

                if (user.Password.Length > 10)
                {
                    Console.Out.WriteLine("Password cannot be longer than 10 characters.");
                    continue;
                }

                hashedString = MD5Hasher.createHashFrom(user.Password);
                if (hashedString == null)
                {
                    Console.Out.WriteLine("\nUnable to create new user");
                    user = null;
                    return;
                }

                user.Password = hashedString;

                break;
            }

            while (true)
            {
                Console.Out.Write("SIN Number: ");
                try
                {
                    user.SINNumber = Console.In.ReadLine();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("Error reading Social Insurance Number");
                    Console.Out.WriteLine("Details: {0}", e.Message);
                    continue;
                }

                if (user.SINNumber.Length > 11)
                {
                    Console.Out.WriteLine("Social Insurance Number cannot be longer than 11 characters.");
                    continue;
                }

                hashedString = MD5Hasher.createHashFrom(user.SINNumber);
                if (hashedString == null)
                {
                    Console.Out.WriteLine("\nUnable to create new user");
                    user = null;
                    return;
                }

                user.SINNumber = hashedString;

                break;
            }

            Console.Out.WriteLine("\nUser successfully created");

        }

        public double checkBalance()
        {
            double currentBalance = 0.0;
            Transaction t = new Transaction();
            List<Transaction> transactions = t.getTransactionsFor(user.PersonId);
            
            foreach (Transaction transaction in transactions)
            {

                TransactionType tt = new TransactionType();
                /*Retrieves the information for this transactionType using the current transaction id value*/
                tt.GetTransactionType(transaction.TransactionTypeId);

                
                if (tt.TransactionDesc.CompareTo("Withdraw") == 0 )
                {
                    currentBalance -= transaction.AmountTransferred;
                }

                if (tt.TransactionDesc.CompareTo("Deposit") == 0)
                {
                    currentBalance += transaction.AmountTransferred;
                }

            }

            return currentBalance;
        }

        public void withdraw(int amount)
        {
            //Need to check user's current balance before money can be withdrawn

            if (amount < 0)
            {
                Console.Out.WriteLine("Amount entered is invalid");
                return;
            }
            
            if (checkBalance() < amount)
            {
                Console.Out.WriteLine("\nInsufficient Funds");
                return;
            }

            TransactionType transType = new TransactionType();
            transType.TransactionDesc = "Withdraw";
            transType.InsertTransactionType();
            List<TransactionType> transTypes = transType.GetTransactionTypes();

            Transaction t = new Transaction();
            t.PersonId = this.user.PersonId;
            /*2 for withdraw, 1 for deposit*/
            t.TransactionTypeId = transTypes[transTypes.Count-1].TransactionTypeId;
            t.AmountTransferred = amount;
            t.InsertTransaction();

        }

        public void deposit(int amount)
        {

            if (amount < 0)
            {
                Console.Out.WriteLine("Amount entered is invalid");
                return;
            }

            TransactionType transType = new TransactionType();
            transType.TransactionDesc = "Deposit";
            transType.InsertTransactionType();
            List<TransactionType> transTypes = transType.GetTransactionTypes();
            

            Transaction t = new Transaction();
            t.PersonId = this.user.PersonId;
            /*2 for withdraw, 1 for deposit*/
            t.TransactionTypeId = transTypes[transTypes.Count-1].TransactionTypeId;
            t.AmountTransferred = amount;
            t.InsertTransaction();
        }

        public void createReport(int toReport, string fileName, int personId)
        {
            
            string sqlQuery = "SELECT TOP " + toReport + " TransactionId, PersonId, TransactionTypeId, AmountTransferred FROM [Transactions] WHERE PersonId =  '" + personId + "'";
            string fileLocation = Directory.GetCurrentDirectory() + "\\" + fileName + ".xml";
            List<Transaction> transactions = new List<Transaction>();
            SqlConnection connection = new SqlConnection(ConnectionString.ATMconnectionString);
            SqlCommand command = new SqlCommand(sqlQuery,connection);
            SqlDataReader reader = null;
            XmlDocument xmlDoc = new XmlDocument();
            XmlNode transactionsNode = null;
            XmlNode transactionNode = null;
            StreamWriter writer = new StreamWriter(fileLocation,false);
            writer.AutoFlush = true;


            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {   
                    Transaction t = new Transaction();
                    t.TransactionId = Convert.ToInt32(reader["TransactionId"]);
                    t.PersonId = Convert.ToInt32(reader["PersonId"]);
                    t.TransactionTypeId = Convert.ToInt32(reader["TransactionTypeId"]);
                    t.AmountTransferred = Convert.ToInt32(reader["AmountTransferred"]);

                    TransactionType tt = new TransactionType();
                    tt.GetTransactionType(t.TransactionTypeId);
                    if (tt.TransactionDesc == "Withdraw")
                    {
                        t.AmountTransferred *= -1;
                    }

                    transactions.Add(t);

                }

            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nUnable to retrieve any transactions for this user");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                {
                    reader.Close();
                }

                connection.Close();
            }


            if (transactions.Count == 0)
            {
                Console.Out.WriteLine("\nUser has not completed any transactions");
                return;
            }

            transactionsNode = xmlDoc.CreateNode("element", "Transactions", "ATM");
            Console.Out.WriteLine("Printing out first " + toReport + " transactions....");

            foreach (Transaction t in transactions)
            {
                Console.Out.WriteLine("TransactionId: {0} -- PersonId: {1} -- TransactionTypeId {2} -- AmountTransferred: {3}",t.TransactionId,t.PersonId,t.TransactionTypeId,t.AmountTransferred);

                transactionNode = xmlDoc.CreateNode("element", "Transaction", "ATM.transactionNodes");
                XmlAttribute TransactionIdAttrib = xmlDoc.CreateAttribute("TransactionId");
                TransactionIdAttrib.Value = t.TransactionId.ToString();
                XmlAttribute PersonIdAttrib = xmlDoc.CreateAttribute("PersonId");
                PersonIdAttrib.Value = t.PersonId.ToString();
                XmlAttribute TransactionTypeIdAttrib = xmlDoc.CreateAttribute("TransactionTypeId");
                TransactionTypeIdAttrib.Value = t.TransactionTypeId.ToString();
                XmlAttribute AmountTransferredAttrib = xmlDoc.CreateAttribute("AmountTransferred");
                AmountTransferredAttrib.Value = t.AmountTransferred.ToString();

                transactionNode.Attributes.Append(TransactionIdAttrib);
                transactionNode.Attributes.Append(PersonIdAttrib);
                transactionNode.Attributes.Append(TransactionTypeIdAttrib);
                transactionNode.Attributes.Append(AmountTransferredAttrib);
                transactionsNode.AppendChild(transactionNode);
            }

            xmlDoc.AppendChild(transactionsNode);

            try
            {
                writer.Write(xmlDoc.InnerXml);
                Console.Out.WriteLine("Report {0}.xml created at {1}", fileName, fileLocation);
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Unable to create report");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
               writer.Close();
            }
           
        }

        

        public void closeAccount()
        {
            Transaction t = new Transaction();
            List<Transaction> transactions = t.getTransactionsFor(user.PersonId);
            foreach (Transaction transaction in transactions){
                TransactionType tt = new TransactionType();
                tt.GetTransactionType(transaction.TransactionTypeId); 
                tt.DeleteTransactionType();
                transaction.DeleteTransaction();
            }

            user.DeletePerson();
            Console.Out.WriteLine("\nAccount closed");
        }

    }
}
