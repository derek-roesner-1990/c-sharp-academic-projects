﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Assignment1
{
    class AdminManager
    {
        public Administrator administrator;
        /*Hashed value of 'nimdaadmin', this password would have to be memorized*/
        public string adminCreationPassword = "zhfprzxK1jbCaiQLDixzyw==";


        public AdminManager()
        {

        }

        public AdminManager(string username, string password)
        {
            administrator = new Administrator();
            administrator.Login(username,password);
        }

        public void createAdmin()
        {
            string hashedString;

            administrator = new Administrator();
            Console.Out.WriteLine("\nPlease enter administrator information");
            while (true)
            {
                Console.Out.Write("Username: ");
                try
                {
                    administrator.username = Console.In.ReadLine();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("\nError reading username");
                    Console.Out.WriteLine("Details: {0}", e.Message);
                    continue;
                }

                if (administrator.username.Length > 20)
                {
                    Console.Out.WriteLine("\nThe administrator user name cannot be longer than 20 characters.");
                    continue;
                }
                break;
            }

            
            while (true)
            {
                Console.Out.Write("Password: ");
                try
                {
                    administrator.password = Console.In.ReadLine();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("\nError reading password");
                    Console.Out.WriteLine("Details: {0}", e.Message);
                    continue;
                }

                if (administrator.password.Length > 20)
                {
                    Console.Out.WriteLine("\nPassword cannot be longer than 20 characters.");
                    continue;
                }

                hashedString = MD5Hasher.createHashFrom(administrator.password);
                if (hashedString == null)
                {
                    Console.Out.WriteLine("\nUnable to create new administrator account");
                    administrator = null;
                    return;
                }

                administrator.password = hashedString;

                break;
            }

            Console.Out.WriteLine("\nNew administrator account successfully created");

        }

        public void backupDatabase()
        {
            /*Since the whole table is being affected and not just the administrator table, no administrator object is needed*/
             /*cannot access current directory due to improper permissions*/
            /*string currentDir = Directory.GetCurrentDirectory();*/
            Console.Out.WriteLine("Attempting to backup file at C:\\\\");
            SqlConnection connection = new SqlConnection(ConnectionString.ADMINconnectionString);
            
            string sqlQuery = "BACKUP DATABASE ATM TO DISK =" + "'C:\\\\ATM.bak'" +" WITH FORMAT;";
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
                Console.Out.WriteLine("Backup of datebase ATM successfully created.");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Unable to backup database");
                Console.Out.WriteLine("Details: {0}",ex.Message);
            }
            finally
            {
                connection.Close();
            }

           
        }

        public void restoreDatabase()
        {
            /*Since the whole table is being affected and not just the administrator table, no administrator object is needed*/ 
            /*string currentDir = Directory.GetCurrentDirectory();*/
            Console.Out.WriteLine("Attempting to restore database from C:\\\\ATM.bak");
            SqlConnection connection = new SqlConnection(ConnectionString.ADMINconnectionString);
            
            /*RESTORE DATABASE AdventureWorks2012 FROM AdventureWorks2012Backups; */

            string useQuery = "USE master";
            string restoreQuery = "RESTORE DATABASE ATM FROM DISK =" + "'C:\\\\ATM.bak'" + ";";
            
            SqlCommand useCommand = new SqlCommand(useQuery, connection);
            SqlCommand restoreCommand = new SqlCommand(restoreQuery, connection);
            try
            {
                connection.Open();
                useCommand.ExecuteNonQuery();
                restoreCommand.ExecuteNonQuery();
                Console.Out.WriteLine("Database ATM successfully restored.");
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Unable to restore database");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void deleteDatabase()
        {
            /*Since the whole table is being affected and not just the administrator table, no administrator object is needed*/
            Console.Out.WriteLine("Attempting to delete database ATM");
            SqlConnection connection = new SqlConnection(ConnectionString.ATMconnectionString);

            string useQuery = "USE master";
            string dropQuery = "DROP DATABASE ATM";
            SqlCommand useCommand = new SqlCommand(useQuery, connection);
            SqlCommand dropCommand = new SqlCommand(dropQuery, connection);

            try
            {
                connection.Open();
                useCommand.ExecuteNonQuery();
                dropCommand.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("Unable to delete database");
                Console.Out.WriteLine("Details: {0}", ex.Message);

            }
            finally
            {
                connection.Close();
            }
        }
    }
}
