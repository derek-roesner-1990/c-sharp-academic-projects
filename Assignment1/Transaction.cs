﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Assignment1
{
    class Transaction
    {
        public string connectionString = ConnectionString.ATMconnectionString;
 
        public int TransactionId
        {
            get;
            set;
        }

        public int PersonId
        {
            get;
            set;
        }

        public int TransactionTypeId
        {
            get;
            set;
        }

        public double AmountTransferred
        {
            get;
            set;
        }

        public Transaction()
        {
            this.PersonId = 0;
            this.TransactionTypeId = 0;
            this.AmountTransferred = 0.0; 
        }

        public void InsertTransaction()
        {
            string sqlQuery = "INSERT INTO [Transactions] (PersonId,TransactionTypeId,AmountTransferred) VALUES ('" + this.PersonId + "','" + this.TransactionTypeId + "','" + this.AmountTransferred + "')";
            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                Console.Out.WriteLine("\nCannot insert this transaction record into the database");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        /*Don't think this should be available - Shouldn't be able to change transaction records*/ 
        public void UpdateTransaction(int personId, int transTypeId, double amountTransferred)
        {
            string sqlQuery = "UPDATE [Transactions] SET  PersonId = '" + personId + "', TransactionTypeId = '" + transTypeId + "', AmountTransferred = '" + amountTransferred + '"';

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nCannot update this transaction record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void DeleteTransaction()
        {
            string sqlQuery = "DELETE FROM [Transactions] WHERE TransactionId ='" + this.TransactionId + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            /* SqlDataReader reader = null;*/

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nCannot delete this transaction record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                /*if (reader != null)
                    reader.Close();*/

                connection.Close();
            }
        }

        public void GetTransaction(int transactionId)
        {

            string sqlQuery = "SELECT * FROM [Transactions] WHERE TransactionId ='" + transactionId + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = command.ExecuteReader();

                if (reader.Read())
                {
                    this.TransactionId = transactionId;
                    this.PersonId = (int)reader["PersonId"];
                    this.TransactionTypeId = (int)reader["TransactionTypeId"];
                    this.AmountTransferred = (int)reader["AmountTransferred"];
                }
                else
                {
                    Console.Out.WriteLine("\nUnable to retrieve transaction record with id {0}", transactionId);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nError retrieving transaction record");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }
        }

        public List<Transaction> GetTransactions()
        {
            List<Transaction> transactionCollection = new List<Transaction>();

            string sqlQuery = "SELECT * FROM [Transactions]";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();
                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Transaction t = new Transaction();
                    t.TransactionId = (int) reader["TransactionId"];
                    t.PersonId = (int) reader["PersonId"];
                    t.TransactionTypeId = (int) reader["TransactionTypeId"];
                    t.AmountTransferred = (double)reader["AmountTransferred"]; 
                    transactionCollection.Add(t);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nUnable to retrieve all transactions");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }

            return transactionCollection;
        }

        public List<Transaction> getTransactionsFor(int personId){
            
            List<Transaction> transactionCollection = new List<Transaction>();

            string sqlQuery = "SELECT * FROM [Transactions] WHERE PersonId = '" + personId + "'";

            SqlConnection connection = new SqlConnection(this.connectionString);
            SqlCommand command = new SqlCommand(sqlQuery, connection);
            SqlDataReader reader = null;

            try
            {
                connection.Open();

                reader = command.ExecuteReader();

                while (reader.Read())
                {
                    Transaction t = new Transaction();
                    t.TransactionId = Convert.ToInt32(reader["TransactionId"]);
                    t.PersonId = Convert.ToInt32(reader["PersonId"]);
                    t.TransactionTypeId = Convert.ToInt32(reader["TransactionTypeId"]);
                    t.AmountTransferred = Convert.ToDouble(reader["AmountTransferred"]);
                    transactionCollection.Add(t);
                }
            }
            catch (Exception ex)
            {
                Console.Out.WriteLine("\nUnable to retrieve transaction records for the personId {0}", personId);
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                connection.Close();
            }

            return transactionCollection;
        }
    }
}
