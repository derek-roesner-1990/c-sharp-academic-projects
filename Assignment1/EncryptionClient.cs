﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System.Text;

namespace Assignment1
{/*http://blogs.msdn.com/b/alejacma/archive/2008/10/23/how-to-generate-key-pairs-encrypt-and-decrypt-data-with-net-c.aspx?Redirected=true*/
    class EncryptionClient
    {

       

          public void SetKeys()
        {
            // Variables
            CspParameters cspParams = null;
            RSACryptoServiceProvider rsaProvider = null;
            string publicKey = "";
            string privateKey = "";
            StreamWriter pubKeyStream = new StreamWriter(Directory.GetCurrentDirectory() + "\\pubKey.txt");
            StreamWriter privKeyStream = new StreamWriter(Directory.GetCurrentDirectory() + "\\privKey.txt"); 
           
            try 
            {
                // Create a new key pair on target CSP
                cspParams = new CspParameters();
                cspParams.ProviderType = 1; // PROV_RSA_FULL 
                //cspParams.ProviderName; // CSP name
                cspParams.Flags = CspProviderFlags.UseArchivableKey;
                cspParams.KeyNumber = (int)KeyNumber.Exchange;
                rsaProvider = new RSACryptoServiceProvider(cspParams);

                // Export public key
                publicKey = rsaProvider.ToXmlString(false);

                try
                {
                    pubKeyStream.WriteLine(publicKey);
                    pubKeyStream.Flush();
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine("Unable to write to file pubKey.txt");
                    Console.Out.WriteLine("Details: {0}",ex.Message);
                }
                finally
                {
                    pubKeyStream.Close();
                }

                // Export private/public key pair 
                privateKey = rsaProvider.ToXmlString(true);

                try
                {
                    privKeyStream.WriteLine(privateKey);
                    privKeyStream.Flush();
                }
                catch (Exception e)
                {
                    Console.Out.WriteLine("Unable to write to file privKey.txt");
                    throw e;
                }
                finally
                {
                    privKeyStream.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot generate new key pair.");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }

        } // Keys

          /*
           *   static string getMd5Hash(string input)
      {
          // Create a new instance of the MD5CryptoServiceProvider object.
          MD5 md5Hasher = MD5.Create();

          // Convert the input string to a byte array and compute the hash.
          byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(input));

          // Create a new Stringbuilder to collect the bytes
          // and create a string.
          StringBuilder sBuilder = new StringBuilder();

          // Loop through each byte of the hashed data 
          // and format each one as a hexadecimal string.
          for (int i = 0; i < data.Length; i++)
          {
              sBuilder.Append(data[i].ToString("x2"));
          }

          // Return the hexadecimal string.
          return sBuilder.ToString();
      }

           * 
           * 
          */

        // Encrypt a file
          public String Encrypt(byte[] toEncrypt)
          {
              // Variables
              CspParameters cspParams = null;
              RSACryptoServiceProvider rsaProvider = null;
              StreamReader pubKeyReader = new StreamReader(Directory.GetCurrentDirectory() + "\\pubKey.txt");
              StreamReader privKeyReader = new StreamReader(Directory.GetCurrentDirectory() + "\\privKey.txt");
              string publicKey = null;
              string encryptedPassword = null;
              byte[] encryptedBytes = null;

              try 
              {
                  // Select target CSP
                  cspParams = new CspParameters();
                  cspParams.ProviderType = 1; // PROV_RSA_FULL 
                  //cspParams.ProviderName; // CSP name
                  rsaProvider = new RSACryptoServiceProvider(cspParams);

                 // Import public key

                  try
                  {
                      publicKey = pubKeyReader.ReadToEnd();
                  }
                  catch (Exception ex)
                  {
                      Console.Out.WriteLine("Unable to read from file pubKey.txt");
                      Console.Out.WriteLine("Details: {0}", ex.Message);
                  }
                  finally
                  {
                      pubKeyReader.Close();
                  }

                  rsaProvider.FromXmlString(publicKey);

               
              // Encrypt plain text
                
                  encryptedBytes = rsaProvider.Encrypt(toEncrypt, false);
                  encryptedPassword = Encoding.Unicode.GetString(encryptedBytes);
               
              }
              catch (Exception ex)
              {
                  Console.WriteLine("Exception encrypting file.");
                  Console.Out.WriteLine("Details: {0}", ex.Message);
              }

              return encryptedPassword;

          } // Encrypt

        // Decrypt a file
        public string Decrypt(byte[] toDecrpyt)
        {
            // Variables
            CspParameters cspParams = null;
            RSACryptoServiceProvider rsaProvider = null;
            StreamReader privKeyReader = new StreamReader(Directory.GetCurrentDirectory() + "privKey.txt");

            string privateKeyText = "";
            string decryptedText = "";
            byte[] plainBytes = null;

            try
            {
                // Select target CSP
                cspParams = new CspParameters();
                cspParams.ProviderType = 1; // PROV_RSA_FULL 
                //cspParams.ProviderName; // CSP name
                rsaProvider = new RSACryptoServiceProvider(cspParams);

                // Read private/public key pair from file
                try
                {
                    privateKeyText = privKeyReader.ReadToEnd();
                }
                catch (Exception ex)
                {
                    Console.Out.WriteLine("Unable to read from privKey.txt");
                    Console.Out.WriteLine("Details: {0}", ex.Message);
                }
                finally
                {
                    privKeyReader.Close();
                }

                // Import private/public key pair
                rsaProvider.FromXmlString(privateKeyText);

               // Decrypt text
                plainBytes = rsaProvider.Decrypt(toDecrpyt, false);
                decryptedText = Encoding.Unicode.GetString(plainBytes);
               
            }
            catch (Exception ex)
            {
                // Any errors? Show them
                Console.WriteLine("Exception decrypting file");
                Console.Out.WriteLine("Details: {0}", ex.Message);
            }
            return decryptedText;
        } // Decrypt
    }
    }

/*ec.SetKeys();
                       string encryptedPass = ec.Encrypt(Encoding.Unicode.GetBytes(newUser.Password));
                   
                       if (encryptedPass == null)
                       {
                           Console.Out.WriteLine("Password could not be decrypted");
                           return;
                       }
                       newUser.Password = encryptedPass;*/
//will need to be decrypted



/*string encryptedSIN = ec.Encrypt(Encoding.Unicode.GetBytes(newUser.SINNumber));

if (encryptedSIN == null)
{
    Console.Out.WriteLine("Password could not be decrypted");
    return;
 * 
 * newUser.SINNumber = encryptedSIN;
}*/