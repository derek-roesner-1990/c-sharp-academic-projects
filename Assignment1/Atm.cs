﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assignment1
{
    public class Atm
    {
        private UserManager userManager = new UserManager();
        private AdminManager adminManager = new AdminManager();

        public Atm()
        {

        }

        public Person getUser()
        {
            return userManager.user;
        }

        public Administrator getAdmin()
        {
            return adminManager.administrator;
        }

        public int attemptUserLogin()
        {
            string email = null;
            string password = null;

            Console.Out.WriteLine("\nPlease enter your credentials");

            Console.Out.Write("Email Address: ");
            email = Console.In.ReadLine();

            Console.Out.Write("Password: ");
            password = Console.In.ReadLine();

            string hashedPassword = MD5Hasher.createHashFrom(password);
            if (hashedPassword == null)
            {
                Console.Out.WriteLine("\nUnable to recieve password");
                return 2;
            }

            password = hashedPassword;

            //creates a Person object from an existing row in the Persons table and assigns it to the user data member of userManager 
            userManager = new UserManager(email, password);
            if (userManager.user.EmailAddress == string.Empty)
            {
                Console.Out.WriteLine("User Login Unsuccessful");
                return 1;
            }
            Console.Out.WriteLine("\nUser Login Successful");
            return 0;
        }

        public int attemptAdminLogin()
        {

            string username = null;
            string password = null;

            Console.Out.WriteLine("\nPlease enter your credentials");

            Console.Out.Write("Username: ");
            username = Console.In.ReadLine();

            Console.Out.Write("Password: ");
            password = Console.In.ReadLine();

            string hashedPassword = MD5Hasher.createHashFrom(password);
            if (hashedPassword == null)
            {
                Console.Out.WriteLine("\nUnable to recieve password");
                return 2;
            }

            password = hashedPassword;

            //creates a adminstrator object from an existing row in the administrator table and assigns it to the administator data member of userManager
            adminManager = new AdminManager(username, password);
            if (adminManager.administrator.username == string.Empty)
            {
                Console.Out.WriteLine("Administrator Login Unsuccessful");
                return 1;
            }
            Console.Out.WriteLine("\nAdministrator Login Successful");
            return 0;
        }

        public int showUserOptions()
        {
           int result = showNumberedMenu("Please make your selection\n0. Logout\n1. Withdraw Funds\n2. Deposit Funds\n3. Display Account Balance\n4. Create Report\n5. Close Account");
            switch (result)
            {
                case 0:
                    return 1;
                case 1:
                    Console.Out.Write("\nWithdrawal Amount: ");
                    int amount = getIntResponse();
                    userManager.withdraw(amount);
                    break;
                case 2:
                    Console.Out.Write("\nDeposit Amount: ");
                    amount = getIntResponse();
                    userManager.deposit(amount);
                    break;
                case 3:
                    double currentBalance = userManager.checkBalance();
                    Console.Out.WriteLine("\nCurrent balance: {0}", currentBalance);
                    break;
                case 4:
                    while (true)
                    {
                        string recordMenu = "Please make your selection\nHow many records would you like to include in this report.\n1. The last 5\n2. The last 10\n3. The last 15";
                        result = showNumberedMenu(recordMenu);

                        if (result >= 1 && result <= 3)
                        {
                            Console.Out.WriteLine("Name of the report? ");
                            string fileName = getStringResponse();
                            userManager.createReport(result * 5, fileName, userManager.user.PersonId);
                            break;
                        }

                    }
                    break;
                case 5:
                    Console.Out.WriteLine("Enter credentials to delete account.");
                    Console.Out.Write("Email Address: ");
                    string email = getStringResponse();
                    Console.Out.Write("Password: ");
                    string password = getStringResponse();
                    string hashedPassword = MD5Hasher.createHashFrom(password);
                    if (hashedPassword == null)
                    {
                        Console.Out.WriteLine("\nUnable to retrieve password");
                        break;
                    }
                    password = hashedPassword;
                    if (email == userManager.user.EmailAddress && password == userManager.user.Password)
                    {
                        userManager.closeAccount();
                    }
                    Console.Out.WriteLine();
                    return 1;
                default:
                    Console.Out.WriteLine("\nInvalid selection");
                    return 0;

            }
            return 0;
        }

        public int showAdminOptions()
        {
            int result = showNumberedMenu("Please make your selection\n0. Logout\n1. Backup Database\n2. Restore the database from a existing .bak file\n3. Delete the ATM database");
            switch (result)
            {
                case 0:
                    return 1;
                case 1:
                    adminManager.backupDatabase();
                    break;
                case 2:
                    adminManager.restoreDatabase();
                    break;
                case 3:
                    adminManager.deleteDatabase();
                    break;
                default:
                    Console.Out.WriteLine("\nInvalid selection");
                    return 0;
            }
            return 0;
        }

        public void showCreateUserDialog()
        {
            userManager.createUser();
            if (userManager.user == null)
            {

                return;
            }
            userManager.user.InsertPerson();
            //Retrieves the PersonId for the new user, to allow for proper operation of the functions within the userManager class.
            userManager.user.GetPersonByName(userManager.user.FirstName, userManager.user.LastName);
        }

        public void showCreateAdminDialog()
        {
            Console.Out.WriteLine("Enter password to add new administrator");
            string password = Atm.getStringResponse();
            string hashedPassword = MD5Hasher.createHashFrom(password);
            if (hashedPassword != adminManager.adminCreationPassword)
            {
                Console.Out.WriteLine("Password Invalid.\nCannot create new administrator.");
                return;
            }

            adminManager.createAdmin();
            if (adminManager.administrator == null)
            {
                return;
            }
            adminManager.administrator.InsertAdministrator();
        }

        /* options will be a string containing numberered option with newlines between each option. 
        * It is very possible to pass improper strings to this funtion.
        * I could split the sentence at each new line and present only 
        * the valid options I find but I don't think it is nessesary here, 
        * since I know what the formatting for options needs to be. 
        */
        public int showNumberedMenu(string options)
        {
            int result;
            Console.Out.WriteLine(options);
            result = getIntResponse();
            Console.Out.WriteLine();
            return result;
        }

       public static string getStringResponse()
        {
            string result = null;
            try
            {
                result = Console.In.ReadLine();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("\nInvalid input");
                Console.Out.WriteLine("Details: {0}", e.Message);
            }
            return result;
        }

        public static int getIntResponse()
        {
            int result = -1;
            try
            {
                result = Int32.Parse(Console.In.ReadLine());
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("\nInvalid input");
                Console.Out.WriteLine("Details: {0}", e.Message);
            }
            return result;
        }
    }
}
