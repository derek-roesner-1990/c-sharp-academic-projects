﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Assignment1
{
    class MD5Hasher
    {
        public static String createHashFrom(string toHash)
        {
            byte[] hashedBytes = null;

            try
            {
                MD5 hasher = MD5.Create();
                hashedBytes = hasher.ComputeHash(Encoding.Default.GetBytes(toHash));
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Error occured while creating a hash value for string {0}",toHash);
                Console.Out.WriteLine("Details: {0}", e.Message);
            }
            return Convert.ToBase64String(hashedBytes);
        }

    }
}
