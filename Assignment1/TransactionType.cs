﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Assignment1
{
    class TransactionType
    {

      public string connectionString = ConnectionString.ATMconnectionString;

      public int TransactionTypeId
      {
          get;
          set;
      }

      public string TransactionDesc
      {
          get;
          set;
      }

      public TransactionType()
      {
          this.TransactionDesc = string.Empty;
      }

      public void InsertTransactionType()
      {
          
          /*I had to change the column name Transaction to TransactionDesc as Transaction seemed to be a reserved keyword in SQL*/
          string sqlQuery = "INSERT INTO [TransactionType] (TransactionDesc) VALUES ('" + this.TransactionDesc + "')";
          SqlConnection connection = new SqlConnection(this.connectionString);
          SqlCommand command = new SqlCommand(sqlQuery, connection);

          try
          {
              connection.Open();
              command.ExecuteNonQuery();
          }
          catch (Exception ex)
          {
              Console.Out.WriteLine("\nCannot insert this transactionType record into the database");
              Console.Out.WriteLine("Details: {0}", ex.Message);
          }
          finally
          {
              connection.Close();
          }
      }

      public void UpdateTransactionType(int transactionTypeId, string transactionDesc)
      {
          string sqlQuery = "UPDATE [TransactionType] SET TransactionDesc = '" + transactionDesc + '"';

          SqlConnection connection = new SqlConnection(this.connectionString);
          SqlCommand command = new SqlCommand(sqlQuery, connection);

          try
          {
              connection.Open();
              command.ExecuteNonQuery();
          }
          catch (Exception ex)
          {
              Console.Out.WriteLine("\nCannot update the transactionType record");
              Console.Out.WriteLine("Details: {0}", ex.Message);
          }
          finally
          {
              connection.Close();
          }
      }

      public void DeleteTransactionType()
      {
          string sqlQuery = "DELETE FROM [TransactionType] WHERE TransactionTypeId ='" + this.TransactionTypeId + "'";

          SqlConnection connection = new SqlConnection(this.connectionString);
          SqlCommand command = new SqlCommand(sqlQuery, connection);
          /* SqlDataReader reader = null;*/

          try
          {
              connection.Open();

              command.ExecuteNonQuery();
          }
          catch (Exception ex)
          {
              Console.Out.WriteLine("\nCannot delete the transactionType record");
              Console.Out.WriteLine("Details: {0}", ex.Message);
          }
          finally
          {
             connection.Close();
          }
      }

      public void GetTransactionType(int transactionTypeId)
      {

          string sqlQuery = "SELECT * FROM [TransactionType] WHERE TransactionTypeId ='" + transactionTypeId + "'";

          SqlConnection connection = new SqlConnection(this.connectionString);
          SqlCommand command = new SqlCommand(sqlQuery, connection);
          SqlDataReader reader = null;

          try
          {
              connection.Open();

              reader = command.ExecuteReader();

              if (reader.Read())
              {
                  this.TransactionTypeId = transactionTypeId;
                  this.TransactionDesc = reader["transactionDesc"].ToString();
              }
              else
              {
                  Console.Out.WriteLine("\nUnable to retrieve transaction type record with id {0}.", transactionTypeId);
              }
          }
          catch (Exception ex)
          {
              Console.Out.WriteLine("\nError retrieving transactionType record.");
              Console.Out.WriteLine("Details: {0}", ex.Message);
          }
          finally
          {
              if (reader != null)
                  reader.Close();

              connection.Close();
          }
      }

      public List<TransactionType> GetTransactionTypes()
      {
          List<TransactionType> transactionTypeCollection = new List<TransactionType>();

          string sqlQuery = "SELECT * FROM [TransactionType]";

          SqlConnection connection = new SqlConnection(this.connectionString);
          SqlCommand command = new SqlCommand(sqlQuery, connection);
          SqlDataReader reader = null;

          try
          {
              connection.Open();

              reader = command.ExecuteReader();

              while (reader.Read())
              {
                  TransactionType tt = new TransactionType();
                  tt.TransactionTypeId = Convert.ToInt32(reader["TransactionTypeId"]);
                  tt.TransactionDesc = reader["TransactionDesc"].ToString();
                  transactionTypeCollection.Add(tt);
              }
          }
          catch (Exception ex)
          {
              Console.Out.WriteLine("\nError retrieving all transaction type records");
              Console.Out.WriteLine("Details: {0}", ex.Message);
          }
          finally
          {
              if (reader != null)
                  reader.Close();

              connection.Close();
          }

          return transactionTypeCollection;
      }
      

    }

}
