﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            int result;/*Determines the menu that is shown once login/user creation is successful -- 1 == user , 2 == administrator --*/
            int menuType = 0;

            Boolean running = false;
            Boolean loggingIn = false;
            Boolean loggedIn = false;

            Atm atm = new Atm();
           /*Initial menu that is displayed while the ATM is not in use. In a pracitcal use of this you wouldn't be able to close this menu*/
            running = true;
            while (running == true)
            {
                Console.Out.WriteLine();
                string greetingMenu = "Welcome!\n\nPlease make your selection\n0. Quit\n1. Create an account\n2. Log into existing account";
                //showNumberedMenu() will display greetingMenu, recieve integer input from the user, plus insert a blank line beneath the user input. 
                result = atm.showNumberedMenu(greetingMenu);

                if (result == 0)
                {
                    running = false;
                    break;
                }

                if (result == -1 || result > 2)
                {
                    Console.Out.WriteLine("Invalid selection");
                    continue;
                }


                /*Menu for creating or logging into an account*/
                loggingIn = true;
                while (loggingIn == true)
                {
                    if (result == 1) //When the user is creating a new account.
                    {
                        string typeMenu = "Please make your selection\nType of account?\n1. User\n2. Administrator";
                        result = atm.showNumberedMenu(typeMenu);

                        if(result == 1)
                        {
                            atm.showCreateUserDialog();
                            //if initialization is unsuccessful userManager.user will retain it's default value
                            if (atm.getUser() == null)
                            {
                                loggingIn = false;
                                break;
                            }
                            loggedIn = true;
                            menuType = 1;
                            break;
                        }

                        if (result == 2)
                        {
                            atm.showCreateAdminDialog();
                            
                            if (atm.getAdmin() == null)
                            {
                                loggingIn = false;
                                break;
                            }
                            loggedIn = true;
                            menuType = 2;
                            break;
                        }
                        
                     } //When the user has chosen to login into a existing account.
                    else if (result == 2) 
                    {

                        string loginMenu = "Please make your selection\nType of account?\n1. User\n2. Administrator";
                        result = atm.showNumberedMenu(loginMenu);
                        
                        if (result == -1 || result > 2)
                        {
                            Console.Out.WriteLine("\nInvalid Input");
                            break;
                        }

                        /*Login attempt - If login fails the user is brought back to the inital menu*/ 
                        while (true)
                        {
                            int errorValue = -1;
                            
                            if (result == 1)
                            {

                                errorValue = atm.attemptUserLogin();

                                if (errorValue == 0)
                                {
                                    menuType = 1;
                                }

                            }
                            else if (result == 2)
                            {
                                errorValue = atm.attemptAdminLogin();

                                if (errorValue == 0)
                                {
                                    menuType = 2;
                                }
                            }

                            if (errorValue == 0)
                            {
                                loggingIn = false;
                                loggedIn = true;
                                break;
                            }

                            if (errorValue > 0)
                            {
                                loggingIn = false;
                                break;
                            }
                        }//end of login attempt menu
                     }
                 }


                //menu once logged into the program
                while (loggedIn == true)
                {
                    Console.Out.WriteLine();
                    Console.Out.WriteLine();

                    switch (menuType)
                    {   //user menu
                        case 1:

                            result = atm.showUserOptions();
                            if (result == 1)
                            {
                                loggedIn = false;
                            }
                            break;
                        case 2:
                            result = atm.showAdminOptions();
                            if (result == 1)
                            {
                                loggedIn = false;
                            }
                            break;
                    }//menuType switch
                }//while logged in
             }//running loop
        }//end of Main
     }//end of program class
}//end of namespace
