﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {
            int result;
            /*Determines the menu that is shown once login/user creation is successful -- 1 == user , 2 == administrator --*/
            int menuType = 0;
            int amount = 0;

            Boolean running = false;
            Boolean loggingIn = false;
            Boolean loggedIn = false;

            UserManager userManager = new UserManager();
            /*adminManager is not initialized here since it's constructor expects the credentials for a login attempt*/
            AdminManager adminManager = null;

            /*Initial menu that is displayed while the ATM is not in use*/
            running = true;
            while (running == true)
            {
                Console.Out.WriteLine();
                Console.Out.WriteLine("Welcome!\n");
                Console.Out.WriteLine("Please make your selection");
                Console.Out.WriteLine("0. Quit");
                Console.Out.WriteLine("1. Create an account");
                Console.Out.WriteLine("2. Log into existing account");
                result = getIntResponse();
                Console.Out.WriteLine();

                if (result == 0)
                {
                    break;
                }

                if (result > 2)
                {
                    Console.Out.WriteLine("Invalid selection");
                    continue;
                }


                /*Menu for creating or logging into an account*/
                loggingIn = true;
                while (loggingIn == true)
                {
                    if (result == 1)
                    {
                        userManager.createUser();
                        if(userManager.user == null)
                        {
                            loggingIn = false;
                            break;
                        }
                        userManager.user.InsertPerson();
                        //Retrieves the PersonId for the new user, to allow for proper operation of the functions within the userManager class.
                        userManager.user.GetPersonByName(userManager.user.FirstName, userManager.user.LastName);
                        loggedIn = true;
                        menuType = 1;
                        break;
                        
                     }
                    else if (result == 2)
                    {
                        Console.Out.WriteLine("Type of account?");
                        Console.Out.WriteLine("1. User");
                        Console.Out.WriteLine("2. Administrator");
                        result = getIntResponse();
                        Console.Out.WriteLine();

                        if (result > 2)
                        {
                            Console.Out.WriteLine("\nInvalid Input");
                            break;
                        }

                        /*Menu to login*/
                        loggingIn = true;
                        while (loggingIn)
                        {
                            string credential = null;
                            string password = null;

                            Console.Out.WriteLine("\nPlease enter your credentials");
                            if (result == 1)
                            {
                                Console.Out.Write("Email Address: ");
                                credential = Console.In.ReadLine();

                                Console.Out.Write("Password: ");
                                password = Console.In.ReadLine();
                                
                                string hashedPassword = MD5Hasher.createHashFrom(password);
                                if (hashedPassword == null)
                                {
                                    Console.Out.WriteLine("\nUnable to recieve password");
                                    loggingIn = false;
                                    break;
                                }

                                password = hashedPassword;
                            }
                            else if (result == 2)
                            {
                                Console.Out.Write("Username: ");
                                credential = getStringResponse();
                            
                                Console.Out.Write("Password: ");
                                password = getStringResponse();

                                
                            }
                            if (result == 1)
                            {
                                //creates a Person object from an existing row in the Persons table and assigns it to the user data member of userManager 
                                userManager = new UserManager(credential, password);
                                Console.Out.WriteLine();
                                if (userManager.user.EmailAddress == string.Empty)
                                {
                                    Console.Out.WriteLine("\nUser Login Unsuccessful");
                                    loggingIn = false;
                                    break;
                                }
                                Console.Out.WriteLine("\nUser Login Successful");
                                menuType = 1;
                                loggingIn = false;
                                loggedIn = true;
                                break;
                            }
                            else if (result == 2)
                            {
                                //creates a adminstrator object from an existing row in the administrator table and assigns it to the administator data member of userManager
                                adminManager = new AdminManager(credential, password);
                                Console.Out.WriteLine();
                                if (adminManager.administrator.username == string.Empty)
                                {
                                    Console.Out.WriteLine("\nAdministrator Login Unsuccessful");
                                    loggingIn = false;
                                    break;
                                }
                                Console.Out.WriteLine("\nAdministrator Login Successful");
                                menuType = 2;
                                loggingIn = false;
                                loggedIn = true;
                                break;
                            }

                            

                        }//end of login menu
                    }
                 }


                //menu once logged into the program
                while (loggedIn == true)
                {
                    Console.Out.WriteLine();
                    Console.Out.WriteLine();

                    switch (menuType)
                    {
                        case 1:
                            Console.Out.WriteLine("Please make your selection");
                            Console.Out.WriteLine("\n0. Quit\n1. Withdraw Funds\n2. Deposit Funds\n3. Display Account Balance\n4. Create Report\n5. Close Account");
                            result = getIntResponse();
                            switch (result)
                            {
                                case 0:
                                    loggedIn = false;
                                    break;
                                case 1:
                                    Console.Out.WriteLine("\nWithdrawal Amount: ");
                                    amount = getIntResponse();
                                    userManager.withdraw(amount);
                                    break;
                                case 2:
                                    Console.Out.WriteLine("\nDeposit Amount: ");
                                    amount = getIntResponse();
                                    userManager.deposit(amount);
                                    break;
                                case 3:
                                    double currentBalance = userManager.checkBalance();
                                    Console.Out.WriteLine("\nCurrent balance: {0}", currentBalance);
                                    break;
                                case 4:
                                    while (true)
                                    {
                                        Console.Out.WriteLine("How many records would you like to include in this report.\n1.The last 5\n2.The last 10\n3.The last 15");
                                        result = getIntResponse();
                                        if (result >= 1 && result <= 3)
                                        {
                                            Console.Out.WriteLine("Name of the report? ");
                                            string fileName = getStringResponse();
                                            userManager.createReport(result * 5, fileName,userManager.user.PersonId);
                                            break;
                                        }

                                    }
                                    break;
                                case 5:
                                    Console.Out.WriteLine("Enter credentials to delete account.");
                                    Console.Out.WriteLine("Email Address: ");
                                    string email = getStringResponse();
                                    Console.Out.WriteLine("Password: ");
                                    string password = getStringResponse();
                                    string hashedPassword = MD5Hasher.createHashFrom(password);
                                    if (hashedPassword == null)
                                    {
                                        Console.Out.WriteLine("\nUnable to retrieve password");
                                        break;
                                    }
                                    password = hashedPassword;
                                    if (email == userManager.user.EmailAddress && password == userManager.user.Password)
                                    {
                                        userManager.closeAccount();
                                    }
                                    Console.Out.WriteLine();
                                    loggedIn = false;
                                    break;
                                 default:
                                    Console.Out.WriteLine("\nInvalid selection");
                                    continue;

                            }
                            break;
                        case 2:
                            Console.Out.WriteLine("Please make your selection");
                            Console.Out.WriteLine("0.Quit\n1.Backup Database\n2.Restore the database from a existing .bak file\n3.Delete the database");
                            result = getIntResponse();
                            switch (result)
                            {
                                case 0:
                                    loggedIn = false;
                                    return;
                                 case 1:
                                    adminManager.backupDatabase();
                                    break;
                                case 2:
                                    adminManager.restoreDatabase();
                                    break;
                                case 3:
                                    adminManager.deleteDatabase();
                                    break;
                                default:
                                    Console.Out.WriteLine("\nInvalid selection");
                                    continue;
                            }
                            break;
                    }//menuType switch
                }//while logged into a account loop
             }//running loop
        }//end of Main


        static int showInitalMenu()
        {

        }


        static string getStringResponse()
        {
            string result = null;
            try
            {
                result = Console.In.ReadLine();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("\nInvalid input");
                Console.Out.WriteLine("Details {0}", e.Message);
            }
            return result;
        }

        static int getIntResponse()
        {
            int result = -1;
            try
            {
                result = Int32.Parse((Console.In.ReadLine()));
            }
            catch(Exception e)
            {
                Console.Out.WriteLine("\nInvalid input");
                Console.Out.WriteLine("Details {0}", e.Message);
            }
            return result;
        }

     }//end of program class
}//end of namespace
