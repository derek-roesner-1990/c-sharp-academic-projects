﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public void submitBtn_Click(Object sender, EventArgs e)
    {
        List<string> gamesPlayed = new List<string>();
        Lab6DataContext lab6DC = new Lab6DataContext();
        
        User user = new User();
        user.FirstName = fname.Text;
        user.LastName = lname.Text;
        user.Birthday = bDayCalender.SelectedDate.Year.ToString() + bDayCalender.SelectedDate.Month.ToString() + bDayCalender.SelectedDate.Day.ToString();
        user.Position = posList.SelectedItem.Text;
        
        user.Telephone = telNum.Text;
        user.Telephone = user.Telephone.Replace("-", "");
        user.Program = program.Text;
       
        for(int i = 0; i < gameCheckList.Items.Count; i++)
        {
            if (gameCheckList.Items[i].Selected == true)
            {
                gamesPlayed.Add(gameCheckList.Items[i].Text);
            }
        }

        for(int i = 0; i < gamesPlayed.Count; i++){
            user.GamesPlayed = user.GamesPlayed + gamesPlayed[i] + " ";
        }

        user.Description = pInfo.Text;

        lab6DC.Users.InsertOnSubmit(user);
        lab6DC.SubmitChanges();
       }
}