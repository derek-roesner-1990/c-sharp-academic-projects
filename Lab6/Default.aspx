﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Theme="Standard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server"> 
    <asp:ValidationSummary ID="ValidationSummary1" runat="server" HeaderText="Whoops!" DisplayMode="BulletList" ForeColor="Green" Font-Size="16px" Font-Bold="true" BorderStyle="Inset" BorderWidth="3px" BorderColor="White"/>
    <br />
   <table>
        <tr>
            <td>First Name</td>
            <td><asp:TextBox Id="fname" runat="server"></asp:TextBox></td>
            <asp:RequiredFieldValidator ID="fNameValidator" runat="server" ControlToValidate="fName" ErrorMessage="First Name Required" Display="None"/>
        </tr>
        <tr> 
            <td>Last Name</td>
            <td><asp:TextBox Id="lname" runat="server"></asp:TextBox></td>
             <asp:RequiredFieldValidator ID="lNameValidator" runat="server" ControlToValidate="lName" ErrorMessage="Last Name Required" Display="None"/>
        </tr>
        <tr>
            <td>Position</td>
            <td><asp:DropDownList Id="posList" runat="server">
                <asp:ListItem Text="Student"/>
                <asp:ListItem Text="Staff"/>
            </asp:DropDownList></td>
        </tr>
        <tr> 
            <td>Telephone Number</td>
            <td><asp:TextBox Id="telNum" runat="server"></asp:TextBox></td>
            <asp:RequiredFieldValidator ID="telValidator" runat="server" ControlToValidate="telNum" ErrorMessage="Telephone Number Required" Display="None"/>
            <asp:RegularExpressionValidator ID="telPatternValidator" runat="server" EnableClientScript="true" ControlToValidate="telNum"  ErrorMessage="Invalid Input for Phone Number - xxx-xxx-xxxx expected." ValidationExpression="^\d{3}-\d{3}-\d{4}$" Display="None" />
        </tr>
        <tr> 
            <td>Program of Study</td>
            <td><asp:TextBox Id="program" runat="server"></asp:TextBox></td>
            <asp:RequiredFieldValidator ID="progValidator" runat="server" ControlToValidate="program" ErrorMessage="Program Name Required" Display="None"/>
        </tr>
        <tr> 
            <td>What's your story?</td>
            <td><asp:TextBox Id="pInfo" runat="server" TextMode="MultiLine" Width="300px" Height="100px" ></asp:TextBox></td>
            <asp:RequiredFieldValidator ID="pInfoValidator" runat="server" ControlToValidate="pInfo" ErrorMessage="Wait up, you forgot to tell your story!" Display="None"/>
       </tr>
       <tr>
            <td>
                <b>Date of Birth:</br>
                <asp:Calendar runat="server" Id="bDayCalender" BackColor="#5C3317" DayStyle-Font-Bold="true" DayStyle-BackColor="Green" DayStyle-ForeColor="White" DayHeaderStyle-BackColor="Green"> </asp:Calendar>                            
            </td>
            <td>
                <br />
                <b><u>You ever play any of these games?</u></b>
                <asp:CheckBoxList runat="server" id="gameCheckList" BackColor="Black" ForeColor="White" TextAlign="Left">
                    <asp:ListItem Text="Super Mario Allstars"/>
                    <asp:ListItem Text="Call of Duty - Black Ops"/>
                    <asp:ListItem Text="Pokemon Gold/Silver"/>
                    <asp:ListItem Text="Donkey Kong 64"/>
                    <asp:ListItem Text="Max Payne"/>
                    <asp:ListItem Text="Sonic and Knuckles"/>
                    <asp:ListItem Text="Doom 3"/>
                    <asp:ListItem Text="World of Warcraft"/>
                    <asp:ListItem Text="NHL 2013"/>
                </asp:CheckBoxList> 
            </td> 
        </tr>
     </table>
     <br />
     <asp:Button runat="server" id="submit" OnClick="submitBtn_Click" Width="100px" Height="60px"></asp:Button>
</asp:Content>
