﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

    public class Task1
    {

        static void Main(string[] args)
        {
            Task1 prog = new Task1();
            Boolean running = true;
            String response;
            
            /*
             * Need to  
             * 
             * Create a C# .NET console application that will:

                Read the text file ‘input.txt’. Display its contents to the console. 

                Write a file called ‘output.txt’ to your desktop. This file will contain the text from the input file modified so that every other character is in upper case.

                A menu should be presented to the user so they can decide which action to take when the application launches. This menu should continue to appear until the user wishes to exit.

                Namespace(s):	
                System
                System.IO;

                Classes of interest:
                string
                Convert
                File
                Static method File.Open()
                File.Close()
                FileStream
                StreamReader 
                or TextReader (Personally I would use StreamReader)
                StreamReader.Read(), ReadLine(), etc
                StreamWriter 
                or TextWriter  (Personally I would use StreamWriter)
                StreamWriter.Write(), WriteLine(), etc

                NOTE: If you open a file/text stream Close() it when you are finished using it. You do not want random files all over the place that you can no longer access.

    
             * 
             */

           while(running == true){

               Console.Out.WriteLine(" 1. Read contents of input.txt and display to console");
               Console.Out.WriteLine(" 2. Read input.txt and write it contents to output.txt on the desktop");
               Console.Out.WriteLine(" 3. Exit");
               response = Console.In.ReadLine();
               Console.Out.WriteLine();
              
                switch (response){
                   case "1":
                      Console.Out.WriteLine(prog.ReadContents());
                      Console.Out.WriteLine();
                        break;
                   case "2":
                      prog.WriteContents(prog.ModifyContents(prog.ReadContents()));
                        break;
                   case "3":
                        running = false;
                        break;
               }


            }
        }//end of main
    
         public String ReadContents()
        {
            String fileContents = "";
            System.IO.StreamReader reader = null;

            try
            {
                reader = new System.IO.StreamReader("C:\\Users\\Derek Roesner\\Desktop\\.Net Programming\\Lab2\\input.txt");
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Unable to create file stream for reading input.txt");
                Console.Out.WriteLine(e.Message);
            }

            while (reader.EndOfStream != true)
            {
                fileContents = fileContents.Insert(fileContents.Length, reader.ReadLine());
            }

            reader.Close();
            return fileContents;
        }

        public String ModifyContents(String toModify)
        {
            int i;
            String currentLetter;

            for (i = 0; i < toModify.Length; i += 2)
            {
                currentLetter = toModify.Substring(i, 1);
                currentLetter = currentLetter.ToUpper();
                toModify = toModify.Remove(i,1);
                toModify = toModify.Insert(i,currentLetter);
            }

            return toModify;
        }

        public void WriteContents(String toWrite)
        {
            System.IO.StreamWriter writer;

            try
            {
                writer = new System.IO.StreamWriter("C:\\Users\\Derek Roesner\\Desktop\\output.txt");
                //ensures that newlines are being printed properly
                writer.Write(toWrite);
                writer.Close();
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("Unable to create a file stream for writing the modified contents of input.txt to the desktop");
                Console.Out.WriteLine(e.Message);
            }

    }//end of class
}
