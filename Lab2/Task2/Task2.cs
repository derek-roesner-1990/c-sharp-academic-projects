﻿using System;
    class Task2
    {
     static void Main(string[] args)
     {

        int size;
        Task2 task2 = new Task2();
        string path = "C:\\\\Windows";
        /*string path = "C:\\Users\\Derek Roesner\\Desktop\\.Net Programming";*/

        Console.Out.WriteLine("{0,-10} {1,-30},{2,-35},{3,-5}","Directory/File Name","Name","Path","Size");
        size = task2.crawl(path);
        Console.Out.WriteLine(String.Format("Total size of directory {0}: {1} mbs.",path,size));
        Console.In.ReadLine();

    }//end of main

        public int crawl(String currentDirectory)
        {
            int i;
            string[] files = null;
            string[] directories = null;
            System.IO.FileInfo fInfo = null;
            System.IO.DirectoryInfo dInfo = null;
            long sizeKb = 0;
            int newMbAmt = 0;
            int sizeMb = 0;

            try
            {
                /*Attempt all these in a row since they all need to be checked for the same error.*/
                directories = System.IO.Directory.GetDirectories(currentDirectory);
                files = System.IO.Directory.GetFiles(currentDirectory);
                dInfo = new System.IO.DirectoryInfo(currentDirectory); 
            }
            catch (UnauthorizedAccessException uae)
            {
                Console.Out.WriteLine(String.Format("Directory {0,-15} -- Cannot Access Directory --", currentDirectory));
            }

            if (directories != null)
            {

                for (i = 0; i < directories.Length; i++)
                {
                   crawl(directories[i]);
                }

            }
            if (files != null)
            {

                for (i = 0; i < files.Length; i++)
                {
                    try
                    {
                        fInfo = new System.IO.FileInfo(files[i]);
                    }
                    catch (UnauthorizedAccessException uae)
                    {
                        Console.Out.WriteLine(String.Format("File {0,-15} -- Cannot Access File --", files[i]));
                    }
                    Console.Out.WriteLine(String.Format("File {0,-30} {1,-35} {2,-5}", fInfo.Name, fInfo.FullName, fInfo.Length));
                    sizeKb += fInfo.Length;
                    newMbAmt = (int)sizeKb / 1024;
                    sizeMb += newMbAmt;
                    sizeKb -= newMbAmt * 1024;
                }

            }

            return sizeMb;
        }
    }

  /*  for(int i = 0; i < directories.Length; i++){
                
                if (directories.Length > 0)
                  {
                    for (int i = 0; i < directories.Length; i++){
                    Console.Out.WriteLine(String.Format("Directory     {0}     {1}  --", directories[0], directories[0]));
                    crawl(directories[i]);
                    files = System.IO.Directory.GetFiles(directories[i]);
             }

                     for (int j = 0; j < files.Length; j++)
                     {
                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(files[j]);
                        Console.Out.WriteLine(String.Format("File  {0} {1} {2}", fileInfo.Name, fileInfo.FullName, fileInfo.Length));
                     }

                }
            
            }
        }*/

        



 





       
    

