﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab4
{
    class Program
    {   
        int fileAmt = 0;
        const int FILE_MAX = 50;

        static void Main(string[] args)
        {
            Program Lab4 = new Program();
           string path = "C:\\Users\\Derek Roesner\\Desktop\\.Net Programming";
           /*string path = "C:\\\\Windows";*/
            Console.Out.WriteLine("Crawling {0}.....", path);
            Lab4.crawl(path);

            File fileManager = new File();
            Console.Out.WriteLine("\nSelecting all files\n");
            List<File> allFiles = fileManager.GetFiles();
            foreach(File f in allFiles)
                Console.Out.WriteLine("{0}/t/t{1}/t/t{2}",f.Name,f.Path,f.Length);
            /*Console.In.ReadLine();*/

            Console.Out.WriteLine("\nDeleting first file");
            allFiles[0].DeleteFile();
            /*Console.In.ReadLine();*/
            
            Console.Out.WriteLine("Selecting all files again\n");
            allFiles = fileManager.GetFiles();
            foreach (File f in allFiles)
                Console.Out.WriteLine("{0}/t/t{1}/t/t{2}", f.Name, f.Path, f.Length);
            /*Console.In.ReadLine();*/
           
            Console.Out.WriteLine("\nSelecting first file\n");
            File firstFile = new File();
            firstFile.GetFile(allFiles[0].FileId);
            Console.Out.WriteLine("{0}\t\t{1}\t\t\t{2}", firstFile.Name, firstFile.Path, firstFile.Length);
            /*Console.In.ReadLine();*/

            Console.Out.WriteLine("\nUpdating first file");
            allFiles[0].UpdateFile("changed", allFiles[0].Path, 10000L);

            /*Console.In.ReadLine();*/
            Console.Out.WriteLine("Re-selecting first file\n");
            allFiles = fileManager.GetFiles();
            firstFile = new File();
            firstFile.GetFile(allFiles[0].FileId);
          

            Console.Out.WriteLine("Updated contents of first file");
            Console.Out.WriteLine("{0}\t\t\t{1}\t\t\t{2}", firstFile.Name, firstFile.Path, firstFile.Length);
            /*Console.In.ReadLine();*/

        }


        public void crawl(String currentDirectory)
        {
            int i;
           
            String[] files = null;
            String[] directories = null;
            System.IO.FileInfo fInfo = null;
            System.IO.DirectoryInfo dInfo = null;
            

            try
            {
                /*Attempt all these in a row since they all need to be checked for the same error.*/
                directories = Directory.GetDirectories(currentDirectory);
                files = Directory.GetFiles(currentDirectory);
                dInfo = new DirectoryInfo(currentDirectory);
            }
            catch (UnauthorizedAccessException uae)
            {
                /*Console.Out.WriteLine(String.Format("Directory {0,-15} -- Cannot Access Directory --", currentDirectory));*/
            }

            if (directories != null)
            {

                for (i = 0; i < directories.Length; i++)
                {
                    if (fileAmt < FILE_MAX)
                    {
                        crawl(directories[i]);
                    }
                }

            }
            if (files != null)
            {

                for (i = 0; i < files.Length; i++)
                {
                    try
                    {
                        fInfo = new FileInfo(files[i]);
                    }
                    catch (UnauthorizedAccessException uae)
                    {
                        /*Console.Out.WriteLine(String.Format("File {0,-15} -- Cannot Access File --", files[i]));*/
                    }
                    if (fInfo != null)
                    {
                        if (fileAmt < FILE_MAX)
                        {
                            File currentFile = new File();
                            currentFile.Name = fInfo.Name;
                            currentFile.Path = fInfo.FullName;
                            currentFile.Length = fInfo.Length;
                            currentFile.InsertFile();
                            fileAmt++;
                        }else{
                            return;
                        }

                     }
                }

            }
        }
    
    }
}
