﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace Lab4
{
    class File
    {
    // holds the objects connection string
    private string connectionString = ConnectionString.Lab4ConnectionString;

    /// <summary>
    /// Property, holds the database id of the object
    /// </summary>
    public int FileId
    {
        get;
        set;
    }

    /// <summary>
    /// Property, holds the first name of the object
    /// </summary>
    public string Name
    {
        get;
        set;
    }

    /// <summary>
    /// Property, holds the last name of the object
    /// </summary>
    public string Path
    {
        get;
        set;
    }

    
   
    /// <summary>
    /// Property, retrieves the ages of the person using the birthday provided to the object
    /// </summary>
    public long Length
    {
       get;
       set;
    }

    /// <summary>
    /// Constructor, default, creates a new empty Person object
    /// </summary>
    public File()
    {
        Name = string.Empty;
        Path = string.Empty;
        Length = 0L;
    }

    /// <summary>
    /// Method, inserts a person to the database
    /// </summary>
    public void InsertFile()
    {
        
        string sqlQuery = "INSERT INTO [File] (Name, Path, Length) VALUES ('" + this.Name + "','" + this.Path + "','"+ this.Length + "')";
      
       SqlConnection connection = new SqlConnection(this.connectionString);
       SqlCommand command = new SqlCommand(sqlQuery, connection);

        try
        {
            connection.Open();

            command.ExecuteNonQuery();
        }
        catch (SqlException ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Method, updates this person object in the database
    /// </summary>
    public void UpdateFile(string name, string path, long length)
    {   
        string sqlQuery = "UPDATE [File] SET Name = '" + name + "', Path = '" + path + "', Length = '" + length + "'";
        
        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);

        try
        {
            connection.Open();

            command.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            connection.Close();
        }
    }

    /// <summary>
    /// Method, deletes this person from the database
    /// </summary>
    public void DeleteFile()
    {
        string sqlQuery = "DELETE FROM [File] WHERE FileId ='" + this.FileId + "'";

        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);
        SqlDataReader reader = null;

        try
        {
            connection.Open();

            command.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (reader != null)
                reader.Close();

            connection.Close();
        }
    }

    /// <summary>
    /// Method, gets a person from the database using the person id provided
    /// </summary>
    public void GetFile(int fileId)
    {
        
        string sqlQuery = "SELECT * FROM [File] WHERE FileId ='" + fileId + "'";
        
        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);
        SqlDataReader reader = null;

        try
        {
            connection.Open();

            reader = command.ExecuteReader();

            if (reader.Read())
            {
                this.FileId = fileId;
                this.Name = reader["Name"].ToString();
                this.Path = reader["Path"].ToString();
                this.Length = Convert.ToInt32(reader["Length"].ToString());
            }
            else
                throw new Exception("File Not Found");
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (reader != null)
                reader.Close();

            connection.Close();
        }
    }

    /// <summary>
    /// Method, gets all the people from the database
    /// </summary>
    /// <returns></returns>
    public List<File> GetFiles()
    {
        List<File> fileCollection = new List<File>();

        string sqlQuery = "SELECT * FROM [FILE]";

        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(sqlQuery, connection);
        SqlDataReader reader = null;

        try
        {
            connection.Open();

            reader = command.ExecuteReader();

            while (reader.Read())
            {
                File f = new File();
                f.FileId = Convert.ToInt32(reader["FileId"]);
                f.Name = reader["Name"].ToString();
                f.Path = reader["Path"].ToString();
                f.Length = Convert.ToInt32(reader["Length"]);

                fileCollection.Add(f);
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            if (reader != null)
                reader.Close();

            connection.Close();
        }

        return fileCollection;
    }

    }
}
