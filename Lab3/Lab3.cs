﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab3
{
    class Lab3
    {
        List<FileInfo> fileInfo = new List<FileInfo>();
        
        public int[] dataset = {84, 21, 46, 84, 99, 1, 76, 56, 84, 37 };
        public List<int> dataList = null;
        
      static void Main(string[] args)
        {

            
            Lab3 lab3 = new Lab3();
            
            /*string path = "C:\\Users\\Derek Roesner\\Desktop\\.Net Programming";*/
            string path = "C:\\\\Windows";
            int response;
            Console.Out.WriteLine("Crawling {0}.....",path);
            lab3.crawl(path);
            

            while (true)
            {
                Console.Out.WriteLine("\n\nOptions\n"+
                                      " 0. Exit\n"+
                                      " 1. Count the number of files in your collection and display the result.\n"+
                                      " 2. Find the largest file in your collection.\n"+
                                      " 3. Find all files with names larger than 10 characters.\n"+
                                      " 4. Order files by size and display.\n"+
                                      " 5. Order files by name and display.\n"+
                                      " 6. Order files by path and display.\n"+
                                      " 7. File all DLLs with unique file names.\n"+
                                      " 8. Display length properties of all files in ascending and descending order.\n"+
                                      " 9. Display the length of the largest file in the collection.\n" +
                                      "10. Display the length of the smallest file in the collection.\n" +
                                      "11. Display the average length of the files in the collection.\n" +
                                      "12. Display the total length of the files in the collection.\n" +
                                      "13. Display the first object in the collection.\n\n\n" +
                                      "\n"+
                                      "14. Find the max number in the data set.\n" +
                                      "15. Find the min number in the data set.\n" +
                                      "16. Find the sum of the numbers in the data set.\n" +
                                      "17. Find the average of the numbers within the data set.\n" +
                                      "18. Find the count of distinct numbers in the data set\n" +
                                      "19. Find the numbers in the dataset with a value greater than 50.\n" +
                                      "20. Select first piece in the data set.\n" +
                                      "21. Convert the data set from int[] to List<int>\n" +
                                      "22. Select dataset and display in descending order\n" +
                                      "23. Find the numbers less than 50, order the results in ascending order and display the top 3 results.");

              

               Console.Out.WriteLine("Please enter you selection:");
               response = Convert.ToInt32(Console.In.ReadLine());
                
               switch (response)
               {    
                    case 0:
                       return;
                       /*Unreachable code*/
                       break;
                    case 1:
                       var result1 = (from fi in lab3.fileInfo select fi).Count();
                       Console.Out.WriteLine("Amount of files in {0}:{1}",path,result1);
                       break;
                   case 2:
                        var result2 = (from fi in lab3.fileInfo orderby fi.Length select fi);
                        Console.Out.WriteLine("Largest file in {0}", path);
                        lab3.printInfo(result2.First());
                        break;
                   case 3:
                       
                       var result3 = (from fi in lab3.fileInfo where fi.Name.Length > 10 select fi);
                       Console.WriteLine("Amount of files with more than 10 characters: {0}", result3.Count()); 
                       foreach (FileInfo fi in result3)
                            lab3.printInfo(fi);

                        break;
                   case 4:
                        Console.WriteLine("File contents of {0} ordered by size",path);
                        var result4 = (from fi in lab3.fileInfo orderby fi.Length descending select fi);
                        foreach (FileInfo fi in result4)
                            lab3.printInfo(fi);
                        break;
                   case 5:
                        Console.WriteLine("File contents of {0} ordered by name",path);
                        var result5 = (from fi in lab3.fileInfo orderby fi.Name descending select fi);
                        foreach (FileInfo fi in result5)
                            lab3.printInfo(fi);
                        break;
                   case 6:
                        Console.WriteLine("File contents of {0} ordered by path name", path); 
                       var result6 = (from fi in lab3.fileInfo orderby fi.FullName descending select fi);
                        foreach (FileInfo fi in result6)
                            lab3.printInfo(fi);
                        break;
                   case 7:
                        var result7 = (from fi in lab3.fileInfo where fi.Name.Contains(".dll") select fi).Distinct();
                        Console.WriteLine("Unique .dll files in {0}: {1}",path,result7.Count());
                        foreach (FileInfo fi in result7)
                            lab3.printInfo(fi);
                        break;
                   case 8:
                        var result8 = (from fi in lab3.fileInfo orderby fi.Length ascending select fi.Length);
                        Console.Out.WriteLine("Files in Ascending Order");
                        foreach (long l in result8)
                            Console.Out.WriteLine("{0,-15}", l);
                        
                        Console.Out.WriteLine();
                        Console.Out.WriteLine();
                        
                        result8 = (from fi in lab3.fileInfo orderby fi.Length descending select fi.Length);
                        Console.Out.WriteLine("Files in Descending Order");
                        foreach (long l in result8)
                            Console.Out.WriteLine("{0,-15}",l);
                        break;
                   case 9:
                        var result9 = (from fi in lab3.fileInfo orderby fi.Length ascending select fi.Length).Max();
                        Console.Out.WriteLine("Size of largest file: {0} kbs",result9);
                        break;
                   case 10:
                         var result10 = (from fi in lab3.fileInfo orderby fi.Length ascending select fi.Length).Min();
                         Console.Out.WriteLine("Size of smallest file: {0} kb(s).", result10);
                        break;
                   case 11:
                        var result11 = (from fi in lab3.fileInfo orderby fi.Length ascending select fi.Length).Average();
                        Console.Out.WriteLine("Average size of file {0:N} kbs.", result11);
                         break;
                   case 12:
                        var result12 = (from fi in lab3.fileInfo orderby fi.Length ascending select fi.Length).Sum();
                        Console.Out.WriteLine("Total size of files is {0} kbs.", result12);
                        break;
                   case 13:
                        var result13 = (from fi in lab3.fileInfo select fi).First();
                        lab3.printInfo(result13);
                        break;
                   case 14:
                        var result14 = (from i in lab3.dataset select i).Max();
                        Console.Out.WriteLine("Max number in the dataset is {0}",result14);
                        break;
                   case 15:
                        var result15 = (from i in lab3.dataset select i).Min();
                        Console.Out.WriteLine("Min number in the dataset is {0}", result15);
                        break;
                   case 16:
                        var result16 = (from i in lab3.dataset select i).Sum();
                        Console.Out.WriteLine("The sum of the values in the dataset is {0}", result16);
                        break;
                   case 17:
                        var result17 = (from i in lab3.dataset select i).Average();
                        Console.Out.WriteLine("The average value in the dataset is {0}", result17);
                        break;
                   case 18:
                        var result18 = (from i in lab3.dataset select i).Distinct();
                        Console.Out.WriteLine("Amount of distinct numbers in the dataset {0}", result18.Count());
                        break;
                   case 19:
                        var result19 = (from i in lab3.dataset where i > 50 select i);
                        Console.Out.WriteLine("Amount of numbers in the dataset greater than 50: {0}", result19.Count());
                        Console.Out.WriteLine("The values are:");
                        foreach (int i in result19)
                        {
                            Console.Out.WriteLine("{0}",i);
                        }
                        break;
                   case 20:
                        var result20 = (from i in lab3.dataset select i).First();
                        Console.Out.WriteLine("The first number in the dataset is {0}", result20);
                        break;
                   case 21:
                        Console.Out.WriteLine("Converting the integer array dataset to a instance of List<int>");
                        try
                        {
                            lab3.dataList = new List<int>();
                            lab3.dataList.AddRange(lab3.dataset);
                            Console.Out.WriteLine("Successfully converted the integer array dataset to a instance of List<int>");
                        }catch(ArgumentNullException argNull){
                            Console.Out.WriteLine("Unable to convert the dataset to a instance of List<int>");
                            Console.Out.WriteLine("Details: {0}");
                        }
                        break;
                   case 22:
                        var result21 = (from i in lab3.dataset orderby i descending select i);
                        Console.Out.WriteLine("The contents of the dataset ordered in descending order");
                        foreach (int i in result21)
                        {
                            Console.Out.WriteLine("{0}",i);
                        }
                        break;
                   case 23:
                        var result22 = (from i in lab3.dataset where i > 50 select i);  
                        var result23 = (from i in result22 orderby i ascending select i).Take(3);
                        Console.Out.WriteLine("The three largest numbers in the dataset greater than 50");
                        foreach (int i in result23)
                        {
                            Console.Out.WriteLine("{0}",i);
                        }
                        break;
                   /*1. Find the Max number in the data set

2. Find the Min number in the data set

3. Find the Sum of the numbers in the data set

4. Find the Average of the numbers in the data set, be sure to include the return type

5. Find the distinct numbers and count the result

6. Find all the numbers with the value greater than 50

7. Select the first number in the data set

8. Convert the data set from int[] to List<int>

9. Select the entire data set and order the results in descending order

10. Find all the numbers less than 50, order the results in ascending order and take the top 3 results. 
   */
                   default:
                        Console.Out.WriteLine("Invalid Input");
                        break;
                 }

               }


          
               
        }

        public int crawl(String currentDirectory)
        {
            int i;
            String[] files = null;
            String[] directories = null;
            System.IO.FileInfo fInfo = null;
            System.IO.DirectoryInfo dInfo = null;
            long sizeKb = 0;
            int newMbAmt = 0;
            int sizeMb = 0;

            try
            {
                /*Attempt all these in a row since they all need to be checked for the same error.*/
                directories = Directory.GetDirectories(currentDirectory);
                files = Directory.GetFiles(currentDirectory);
                dInfo = new DirectoryInfo(currentDirectory); 
            }
            catch (UnauthorizedAccessException uae)
            {
                /*Console.Out.WriteLine(String.Format("Directory {0,-15} -- Cannot Access Directory --", currentDirectory));*/
            }

            if (directories != null)
            {

                for (i = 0; i < directories.Length; i++)
                {
                   crawl(directories[i]);
                }

            }
            if (files != null)
            {

                for (i = 0; i < files.Length; i++)
                {
                    try
                    {
                        fInfo = new FileInfo(files[i]);
                    }
                    catch (UnauthorizedAccessException uae)
                    {
                        /*Console.Out.WriteLine(String.Format("File {0,-15} -- Cannot Access File --", files[i]));*/
                    }
                    if (fInfo != null)
                    {   
                        fileInfo.Add(fInfo);
                        /*Console.Out.WriteLine(String.Format("File {0,-30} {1,-35} {2,-5}", fInfo.Name, fInfo.FullName, fInfo.Length));*/
                        sizeKb += fInfo.Length;
                        newMbAmt = (int)sizeKb / 1024;
                        sizeMb += newMbAmt;
                        sizeKb -= newMbAmt * 1024;
                    }
                }

            }

            return sizeMb;
        }


        public void printInfo(FileInfo fi)
        {
            Console.Out.WriteLine("{0,-40}{1,-10}{2,-50}",fi.Name,fi.Length,fi.Directory);
        }


    }
}
